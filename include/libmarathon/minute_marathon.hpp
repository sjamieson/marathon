//
// Created by stewart on 28/06/22.
//

#include "marathon.hpp"
#include <archive.h>

#ifndef MARATHON_MINUTE_MARATHON_HPP
#define MARATHON_MINUTE_MARATHON_HPP

namespace Marathon {

    class CompressedFileStream {
        FileStream base_stream;

    public:
        CompressedFileStream(std::filesystem::path const& fpath, bip::mode_t const mode, size_t const offset = 0, size_t const size = 0,
                size_t const min_size = 0)
                :base_stream(fpath, mode, offset, size, min_size) {

        }

        void advise(bip::mapped_region::advice_types advice) {
            base_stream.advise(advice);
        }

        void prepareWrite(size_t const write_size) {
            base_stream.prepareWrite(write_size);
        }

        void prepareRead(size_t const read_size = 0) {
            base_stream.prepareRead(read_size);
        }

        bool truncate() {
            return base_stream.truncate();
        }
    };

}

#endif //MARATHON_MINUTE_MARATHON_HPP
