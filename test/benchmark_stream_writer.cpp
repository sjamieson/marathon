#include <iostream>
#include "../include/libmarathon/marathon.hpp"
#include <chrono>
#include <iomanip>
#include <vector>

int main() {
//    std::string const base_dir = "/tmp";
    std::string base_dir = "/home/stewart";
    size_t constexpr num_bytes = 1920 * 1080 * 3 * 100;
    using namespace std::chrono;
    duration<long, std::ratio<1, 1000000000>> allocate_vector_duration{}, vector_time{};
    auto const start_allocate_vector = steady_clock::now();
    {
        std::vector<unsigned char> numbers(num_bytes);

        auto const start = steady_clock::now();
        for (auto i = 0; i < num_bytes; i++) {
            numbers[i] = (unsigned char) (i % 256);
        }
        auto const end = steady_clock::now();
        vector_time = end - start;
    }
    auto const end_allocate_vector = steady_clock::now();
    allocate_vector_duration = end_allocate_vector - start_allocate_vector;
    std::cout << "Calculation time:  " << std::setw(9) << duration_cast<microseconds>(vector_time).count() << "us" << std::endl;
    std::cout << "Total array time:  " << std::setw(9) << duration_cast<microseconds>(allocate_vector_duration).count() << "us" << std::endl;

    std::vector<unsigned char> numbers(num_bytes);
    for (auto i = 0; i < num_bytes; i++) {
        numbers[i] = (unsigned char) (i % 256);
    }

    duration<long, std::ratio<1, 1000000000>> allocate_stream_duration{}, stream_time{};
    std::string const path1 = base_dir + "/test1.mh";
    if (std::filesystem::exists(path1) && !std::filesystem::remove(path1)) throw std::runtime_error("Failed to delete " + path1);
    auto const start_allocate_stream = steady_clock::now();
    {
        Marathon::FileStream outputStream(path1, boost::interprocess::read_write, 0, sizeof(unsigned char) * num_bytes);
        auto const start_stream = steady_clock::now();
        for (size_t i = 0; i < num_bytes; ++i) {
            outputStream.write(numbers[i]);
        }
        auto const end_stream = steady_clock::now();
        stream_time = end_stream - start_stream;
    }
    auto const end_allocate_stream = steady_clock::now();
//    std::filesystem::remove(path1);
    allocate_stream_duration = end_allocate_stream - start_allocate_stream;
    std::cout << "Stream write time: " << std::setw(9) << std::setw(9) << duration_cast<microseconds>(stream_time).count() << "us" << std::endl;
    std::cout << "Stream total time: " << std::setw(9) << std::setw(9) << duration_cast<microseconds>(allocate_stream_duration).count() << "us" << std::endl;

    duration<long, std::ratio<1, 1000000000>> allocate_bulk_stream_duration{}, bulk_stream_time{};
    std::string const path2 = base_dir + "/test2.mh";
    if (std::filesystem::exists(path2) && !std::filesystem::remove(path2)) throw std::runtime_error("Failed to delete " + path2);
    auto const start_allocate_bulk_stream = steady_clock::now();
    {
        Marathon::FileStream outputStream(path2, boost::interprocess::read_write, 0, sizeof(decltype(numbers)::value_type) * num_bytes);
        auto const start_stream = steady_clock::now();
        outputStream.write(numbers);
        auto const end_stream = steady_clock::now();
        bulk_stream_time = end_stream - start_stream;
    }
    auto const end_allocate_bulk_stream = steady_clock::now();
//    std::filesystem::remove(path2);
    allocate_bulk_stream_duration = end_allocate_bulk_stream - start_allocate_bulk_stream;
    std::cout << "Bulk write time:   " << std::setw(9) << duration_cast<microseconds>(bulk_stream_time).count() << "us" << std::endl;
    std::cout << "Bulk total time:   " << std::setw(9) << duration_cast<microseconds>(allocate_bulk_stream_duration).count() << "us" << std::endl;

    duration<long, std::ratio<1, 1000000000>> fileio_time{};
    std::string const path3 = base_dir + "/test3.dat";
    if (std::filesystem::exists(path3) && !std::filesystem::remove(path3)) throw std::runtime_error("Failed to delete " + path2);
    auto const start_stream = steady_clock::now();
    {
        std::ofstream outfile(path3, std::ios_base::out | std::ios_base::binary);
        for (auto const& n : numbers) outfile.write(reinterpret_cast<char const*>(&n), sizeof(n));
    }
    auto const end_stream = steady_clock::now();
    fileio_time = end_stream - start_stream;
//    std::filesystem::remove(path3);
    std::cout << "FileIO time:       " << std::setw(9) << duration_cast<microseconds>(fileio_time).count() << "us" << std::endl;
}
