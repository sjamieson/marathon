#include <iostream>
#include "../include/libmarathon/marathon.hpp"
#include <chrono>

int main() {
    using namespace std::chrono;
    std::string const base_dir = "/tmp";
    std::string const path = base_dir + "/test.mh";
    size_t const num_bytes = 1920 * 1080 * 3;
    std::vector<unsigned char> numbers(num_bytes);
    for (auto i = 0; i < num_bytes; i++) {
        numbers[i] = (unsigned char) (i % 256);
    }
    {
        Marathon::FileStream outputStream(path, boost::interprocess::read_write, 0, sizeof(unsigned char) * num_bytes);
        outputStream.write(numbers);
    }

    std::vector<unsigned char> comparison(num_bytes);
    duration<long, std::ratio<1, 1000000000>> allocate_bulk_stream_duration{}, bulk_stream_time{};
    size_t const buf_size = sizeof(unsigned char) * num_bytes;
    auto const start_allocate_bulk_stream = steady_clock::now();
    {
        Marathon::FileStream inputStream(path, boost::interprocess::read_only);
        if (inputStream.available() != buf_size) std::cout << "Failed buffer availability check!!!" << std::endl;
        if (inputStream.size() != buf_size) std::cout << "Failed buffer size check!!!" << std::endl;
        if (inputStream.eos()) std::cout << "Failed buffer eos check!!!" << std::endl;
        auto const start_read = steady_clock::now();
        inputStream.read(comparison);
        auto const end_read = steady_clock::now();
        if (inputStream.size() != buf_size) std::cout << "Failed buffer size check!!!" << std::endl;
        if (!inputStream.eos()) std::cout << "Failed buffer eos check!!!" << std::endl;
        bulk_stream_time = end_read - start_read;
    }
    auto const end_allocate_bulk_stream = steady_clock::now();
    allocate_bulk_stream_duration = end_allocate_bulk_stream - start_allocate_bulk_stream;
    std::filesystem::remove(path);
    if (comparison != numbers) std::cout << "Failed validation check!!!" << std::endl;
    std::cout << "Bulk read time:   " << duration_cast<microseconds>(bulk_stream_time).count() << "us" << std::endl;
    std::cout << "Bulk total time:  " << duration_cast<microseconds>(allocate_bulk_stream_duration).count() << "us" << std::endl;
    return 0;
}
