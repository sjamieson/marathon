#include <iostream>
#include "../include/libmarathon/marathon.hpp"

using namespace Marathon;

struct Employee {
    std::string name;
    std::string title;
    uint16_t year_joined_whoi;
};

template<class Obj>
class Serializer {};

template<>
struct Serializer<Employee> {
    template<typename Stream, typename T> inline constexpr static void allInOne(Stream& stream, T m)
    {
        stream.next(m.name);
        stream.next(m.title);
        stream.next(m.year_joined_whoi);
    }
    template<typename Stream, typename T> inline constexpr static void write(Stream& stream, T const& m)
    {
        allInOne<Stream, T const&>(stream, m);
    }
    template<typename Stream, typename T> inline constexpr static void read(Stream& stream, T& m)
    {
        allInOne<Stream, T&>(stream, m);
    }
};

int main() {
    using namespace std::chrono;
    size_t const copies = 1000000;
    {
        TableArchive<Employee, Serializer> test("/tmp/marathon", "test", ModeType::TRUNCATE, 32 * 1024, 0);
        auto start = steady_clock::now();
        for (auto i = 0; i < copies; ++i) {
            test.write({"Stewart Jamieson", "PhD Student", 2018});
            test.write({"John San Soucie", "PhD Student", 2018});
            test.write({"Levi Cai", "PhD Student", 2018});
            test.write({"Jessica Todd", "PhD Student", 2020});
            test.write({"Ethan Fahnestock", "PhD Student", 2021});
            test.write({"Daniel Yang", "PhD Student", 2021});
            test.write({"Yogesh Girdhar", "Associate Scientist", 2014});
            test.write({"Seth McCammon", "Postdoctoral Fellow", 2021});
        }
        auto end = steady_clock::now();
        std::cout << "Wrote " << test.size() << " records in " << duration_cast<microseconds>(end - start).count() << " us" << std::endl;
    }

    {
        TableArchive<Employee, Serializer> test("/tmp/marathon", "test", ModeType::READ_ONLY);
        size_t count = 0;
        size_t total_name_len = 0;
        auto start = steady_clock::now();
        while (!test.eof()) {
            auto emp = test.read();
            total_name_len += emp.name.size();
//            std::cout << emp.name << "," << emp.age << "," << emp.height << std::endl;
            count++;
        }
        auto end = steady_clock::now();
        std::cout << "Total name len " << total_name_len << std::endl;
        std::cout << "Read " << test.size() << " records in " << duration_cast<microseconds>(end - start).count() << " us" << std::endl;
        assert(count == copies * 8);
    }

}