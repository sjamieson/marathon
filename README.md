# Marathon

A library to enable rapid data recording to disk, including options for compression. Some optimizations for images.

Ships with libqoi (qoi.h).

## Prerequisites

A compiler that supports C++17, e.g. g++-8

```bash
# e.g., for Ubuntu 18.04
sudo apt install gcc-8 g++-8

# Run these in terminal before installing, or add them to .bashrc
export CC=gcc-8
export CXX=g++-8
```

## Install

```bash
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make
sudo make install
```
