#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <libmarathon/ros_marathon.hpp>

#define QOI_IMPLEMENTATION
#define QOI_MALLOC(sz) reinterpret_cast<unsigned char*>(malloc(sz))
#define QOI_FREE(sz) free(sz)

#include <libqoi/qoi.h>

static size_t count = 0;
static bool use_compression = false;

std::vector<uint8_t> decode(std::vector<uint8_t> data) {
    size_t const len = data.size();
    if (len == 0) return {};
    qoi_desc desc{0, 0, 3, QOI_SRGB};
    void *decoded = qoi_decode(data.data(), len, &desc, 3);
    if (decoded == nullptr || desc.width == 0 || desc.height == 0) {
        return {};
    }
    data.resize(desc.width * desc.height * desc.channels);
    memcpy(data.data(), decoded, data.size());
    return data;
}

#ifdef USE_MARATHON
void imageCallback(sensor_msgs::Image msg)
{
#else
void imageCallback(sensor_msgs::ImageConstPtr const &msgptr) {
    auto msg = *msgptr;
#endif
    std::cout << (ros::Time::now().toSec() - msg.header.stamp.toSec()) << std::endl;
    try {
        if (use_compression) msg.data = decode(msg.data);
        cv::imshow("view", cv_bridge::toCvCopy(msg, "bgr8")->image);
        cv::waitKey(10);
    }
    catch (cv_bridge::Exception &e) {
        ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg.encoding.c_str());
    }
    count++;
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "image_listener");
    if (argc >= 2) use_compression = (bool) std::stoi(argv[1]);
    ros::NodeHandle nh;
    cv::namedWindow("view");
    cv::startWindowThread();
#ifdef USE_MARATHON
    ros_marathon::MarathonTransport mt(nh);
    ros_marathon::Subscriber sub = mt.subscribe("camera/image", 1, imageCallback);
#else
    image_transport::ImageTransport it(nh);
    image_transport::Subscriber sub = it.subscribe("camera/image", 1, imageCallback);
#endif
    ros::spin();
    cv::destroyWindow("view");
}
