#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <libmarathon/ros_marathon.hpp>

#define QOI_IMPLEMENTATION
#define QOI_MALLOC(sz) reinterpret_cast<unsigned char*>(malloc(sz))
#define QOI_FREE(sz) free(sz)
#include <libqoi/qoi.h>

std::vector<uint8_t> encode(cv::Mat const &image) {
    int len = 0;
    qoi_desc const desc{static_cast<unsigned int>(image.cols), static_cast<unsigned int>(image.rows), 3, QOI_SRGB};
    auto const data = static_cast<uint8_t*>(qoi_encode(static_cast<void *>(image.data), &desc, &len));
    if (data == nullptr || len == 0) return {};
    std::vector<uint8_t> out(data, data + len);
    free(data);
    return out;
}

int main(int argc, char** argv)
{
    // Check if video source has been passed as a parameter
    if(argv[1] == NULL) return 1;

    ros::init(argc, argv, "image_publisher");
    ros::NodeHandle nh;
#ifdef USE_MARATHON
    ros_marathon::MarathonTransport mt(nh);
    ros_marathon::Publisher pub = mt.advertise<sensor_msgs::Image>("camera/image", 1);
#else
    image_transport::ImageTransport it(nh);
    image_transport::Publisher pub = it.advertise("camera/image", 1);
#endif
    // Convert the passed as command line parameter index for the video device to an integer
    std::istringstream video_sourceCmd(argv[1]);
    bool compression = (argc > 2) ? (bool) std::stoi(argv[2]) : false;
    size_t max_msgs = (argc > 3) ? std::stoi(argv[3]) : -1;
    int video_source;
    // Check if it is indeed a number
    if(!(video_sourceCmd >> video_source)) return 1;

    cv::VideoCapture cap(video_source);
    // Check if video device can be opened with the given index
    if(!cap.isOpened()) return 1;
    cv::Mat frame;
    sensor_msgs::ImagePtr msg;

    ros::Rate loop_rate(10);
    size_t count = 0;
    while (nh.ok()) {
        if (pub.getNumSubscribers()) {
            cap >> frame;
            // Check if grabbed frame is actually full with some content
            if (!frame.empty()) {
                cv::resize(frame, frame, {1920, 1080}, 0, 0, CV_INTER_LINEAR);
                msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", frame).toImageMsg();
                if (compression) msg->data = encode(frame);
                auto const start = ros::Time::now();
                msg->header.stamp = start;
                pub.publish(msg);
                std::cout << (ros::Time::now().toSec() - start.toSec()) << std::endl;
                cv::waitKey(1);
            } else {
                std::cout << "Empty frame!" << std::endl;
            }
        } else std::cout << "waiting for subscribers" << std::endl;
        if (++count >= max_msgs) break;

        ros::spinOnce();
        loop_rate.sleep();
    }
}

